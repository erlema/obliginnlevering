package CardGame.Data;

import java.util.ArrayList;
/**
 * @author Erlend Matre
 * Class to represent a hand in a card game
 */

public class Hand {
    private ArrayList<PlayingCard> hand= new ArrayList<PlayingCard>(); //The list where the player's cards are stored

    /**
     * resets the deck and gives the player a new hand of cards
     * @param n amount of cards to be dealt
     */
    public void getNewHand(int n){
        DeckOfCards newDeck = new DeckOfCards();
        hand = newDeck.dealHand(n);
    }

    /**
     * method for getting the sum of all faces
     * @return sum of all faces
     */
    public int getSumOfFaces(){
        int totalSum= hand.stream().mapToInt(PlayingCard::getFace).sum();
        return totalSum;
    }

    /**
     * checks if the hand has a flush
     * @return true/false depending on if it has flush
     */
    public Boolean checkFlush(){
        char c = hand.get(0).getSuit();
        for (PlayingCard e: hand) {
            char p = e.getSuit();
            if (!(c==p)){
                return false;
            }
        }
        return true;
    }

    /**
     * checks if the hand has the queen of spades
     * @return  true/false depending on if it has the queen of spades
     */
    public Boolean hasQueenOfSpades(){
        return hand.stream().anyMatch(p->p.getSuit()=='S'&& p.getFace()==12);
    }

    /**
     * gets all the heart cards and returns it as one string
     * @return the heart cards as String or a message if there are no hearts
     */
    public String getCardOfHearts(){ //Did not know a way to use streams to make this easier
        String returnMessage = "";   //because adding string together did not seem to work
        Boolean hasHearts = false;
        for (PlayingCard e: hand) {
            if (e.getSuit() == 'H'){
                returnMessage +=e.getAsString() + " ";
                hasHearts = true;
            }
        }
        if(hasHearts){return returnMessage;}
        return "No hearts";
    }

    /**
     * Returns the hand as a string message
     * @return the hand as string
     */
    public String showHand(){    //Did not know a way to use streams to make this easier
        String cardMessage="";   //because adding string together did not seem to work
        int cardNumber = 1;
        for (PlayingCard p: hand) {
            cardMessage += "Card " + cardNumber +": "+  p.getAsString() + '\n';
            cardNumber ++;
        }
        return cardMessage;
    }

    /**
     * Checks if the hand is empty, is useful in the JavaFX parts
     * @return true/false depending if the hand is empty or not
     */
    public boolean isHandEmpty(){
        if(hand.size()==0) return true;
        else return false;
    }
}
