module ObligInnlevering{
    requires javafx.base;
    requires javafx.controls;
    requires javafx.graphics;
    requires javafx.fxml;
    opens CardGame to javafx.fxml;
    exports CardGame;
}