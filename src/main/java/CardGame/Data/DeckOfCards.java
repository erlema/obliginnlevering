package CardGame.Data;

import java.util.ArrayList;
import java.util.Random;
/**
 * @author Erlend Matre
 * Class to represent a deck of cards
 */
public class DeckOfCards {
    private final char[] suit ={'S','H','D','C'}; //Makes it easier to deal a hand
    private ArrayList<PlayingCard> allCards = new ArrayList<PlayingCard>(); //where the cards are stored

    /**
     * constructor for the class, adds every possible card to the allCards ArrayList
     */
    public DeckOfCards (){
        for(char a : suit){
            int cardNumber = 1;
            for (cardNumber=1; cardNumber<14; cardNumber ++){
                allCards.add(new PlayingCard(a,cardNumber));
            }
        }
    }

    /**
     * method for dealing a hand
     * @param n amount of cards to be dealt
     * @return the hand as an ArrayList
     */
    public ArrayList<PlayingCard> dealHand(int n){
        Random random = new Random();
        int randomNumber;
        ArrayList<PlayingCard> hand = new ArrayList<PlayingCard>();
        if (n<53&&n>0){
            for (int i=0; n>i; i++){
                randomNumber = random.nextInt(52);
                while (hand.contains(allCards.get(randomNumber))){
                randomNumber=random.nextInt(52);
            }
                hand.add(allCards.get(randomNumber));
        }
        //hand.add(new PlayingCard('S',12));
        return hand;}
        return null;
    }
}