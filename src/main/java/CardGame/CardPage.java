package CardGame;

import CardGame.Data.Hand;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;

/**
 * @author Erlend Matre
 * The controller class to the CardPage.fxml
 */
public class CardPage {
    /**
     * All the different components of the fxml file that needs to
     * be changed for the app to show the right information.
     * Also a hand for the player/user
     */
    @FXML
    private Label cards;
    @FXML
    private CheckBox flushCheck;
    @FXML
    private CheckBox queenCheck;
    @FXML
    private Label heartLabel;
    @FXML
    private Label facesLabel;

    private Hand playersHand;

    /**
     * When the page is opened the player gets a new hand, which is empty
     */
    public void initialize() {
        playersHand = new Hand();
    }

    /**
     * Updates the with 5 new cards (could be changed to get an input)
     */
    public void updateHand(){
        playersHand.getNewHand(5);
    }

    /**
     * Updates all the fxml components to show the right information
     */
    public void showHand(){

        heartLabel.setText(playersHand.getCardOfHearts());

        facesLabel.setText(Integer.toString(playersHand.getSumOfFaces()));

        queenCheck.setText("Has Queen of Spades"); //Is needed if the player checks the checkboxes
        flushCheck.setText("Flush"); //Is needed if the player checks the checkboxes

        if (playersHand.checkFlush()){ flushCheck.setSelected(true);}
        else flushCheck.setSelected(false);
        if(playersHand.hasQueenOfSpades()){ queenCheck.setSelected(true);}
        else queenCheck.setSelected(false);

        cards.setText(playersHand.showHand());
    }

    /**
     * Method for handling a button click on the deal hand button
     */
    public void handleDealHandButtonClick() {
       updateHand();
    }

    /**
     * Method for handling a button click on the check hand button
     */
    public void handleCheckHandButtonClick() {
        if(!(playersHand.isHandEmpty())) showHand();
    }

    /**
     * Fun method because it was not possible to disable the check on the checkbox button
     */
    public void handleFlushCheckBoxClick() {
        flushCheck.setText("CHEATER!");
    }

    /**
     * Fun method because it was not possible to disable the check on the checkbox button
     */
    public void handleQueenCheckBoxClick(){
        queenCheck.setText("CHEATER!");
    }
}
