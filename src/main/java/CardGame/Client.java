package CardGame;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

import javafx.stage.Stage;
/**
 * @author Erlend Matre
 * Client class where the application is run
 */
public class Client extends Application {
    /**
     * A abstract method needed after inheriting Application
     * Starts the application
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/CardPage.fxml"));
        primaryStage.setTitle("Card Game");
        primaryStage.setScene(new Scene(root));
        primaryStage.setResizable(false); //
        primaryStage.show();
    }

    /**
     * Main method that launches the application
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }
}